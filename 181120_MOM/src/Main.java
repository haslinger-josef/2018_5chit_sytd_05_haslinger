import CPU.Blocked;
import CPU.JobProducer;
import CPU.Ready;
import CPU.Running;
import message.Consumer;
import message.Producer;

public class Main {

    public static void main(String[] args) {
        Thread jobproducer = new Thread(new JobProducer());
        Thread ready = new Thread(new Ready());
        Thread blocked = new Thread(new Blocked());
        Thread running = new Thread(new Running());

        jobproducer.start();
        ready.start();
        blocked.start();
        running.start();


    }
}
