package CPU;

import java.util.Random;

public class Ready implements Runnable {
    @Override
    public void run() {
        while(true) {
            try {
                Job j = ReadyQueue.getInstance().take();
                System.out.println("Ready: " + j.getContent());

                Random r = new Random();

                if (r.nextInt(10) <= 7) {
                    RunningQueue.getInstance().put(j);
                } else {
                    BlockedQueue.getInstance().put(j);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
