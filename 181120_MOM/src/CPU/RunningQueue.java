package CPU;

import java.util.concurrent.ArrayBlockingQueue;

public class RunningQueue extends ArrayBlockingQueue<Job> {

    private static final RunningQueue instance = new RunningQueue();

    private RunningQueue() {
        super(20);
    }

    public static final RunningQueue getInstance() {
        return instance;
    }
}
