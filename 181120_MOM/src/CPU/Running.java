package CPU;

import java.util.Random;

public class Running implements Runnable{

    @Override
    public void run() {
        while(true) {
            try {
                Job j = RunningQueue.getInstance().take();
                System.out.println("Running: " + j.getContent());

                Random r = new Random();
                if (r.nextInt(10) <= 3) {
                    System.out.println("\tDead "+j.getContent());
                } else {
                    if (r.nextInt(10) > 5) {
                        BlockedQueue.getInstance().put(j);
                    } else {
                        ReadyQueue.getInstance().put(j);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
