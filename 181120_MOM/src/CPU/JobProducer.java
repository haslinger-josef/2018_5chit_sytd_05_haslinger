package CPU;


public class JobProducer implements Runnable {
    @Override
    public void run() {
        for (int k = 0; k <= 7; k++) {
            Job job = new Job("Job " + k);
            try {
                ReadyQueue.getInstance().put(job);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
