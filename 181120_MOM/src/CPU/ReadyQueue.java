package CPU;

import java.util.concurrent.ArrayBlockingQueue;

public class ReadyQueue extends ArrayBlockingQueue<Job> {
    private static final ReadyQueue instance = new ReadyQueue();

    private ReadyQueue() {
        super(20);
    }

    public static final ReadyQueue getInstance() {
        return instance;
    }
}
