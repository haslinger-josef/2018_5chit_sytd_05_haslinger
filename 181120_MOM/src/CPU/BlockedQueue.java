package CPU;

import message.Message;
import message.MessageQueue;

import java.util.concurrent.ArrayBlockingQueue;

public class BlockedQueue extends ArrayBlockingQueue<Job> {
    private static final BlockedQueue instance = new BlockedQueue();

    private BlockedQueue() {
        super(20);
    }

    public static final BlockedQueue getInstance() {
        return instance;
    }
}
