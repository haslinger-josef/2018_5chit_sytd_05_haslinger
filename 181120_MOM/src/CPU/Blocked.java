package CPU;

import jdk.nashorn.internal.ir.Block;

import java.util.Random;

public class Blocked implements Runnable {

    @Override
    public void run() {
        while(true) {
            try {
                Job j = BlockedQueue.getInstance().take();
                System.out.println("Blocked: " + j.getContent());

                Random r = new Random();
                if (r.nextInt(10) >= 8) {
                    BlockedQueue.getInstance().put(j);
                    System.out.println("Stays Blocked: "+j.getContent());
                } else {
                    ReadyQueue.getInstance().put(j);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

