package message;

import java.util.concurrent.ArrayBlockingQueue;

public class MessageQueue extends ArrayBlockingQueue<Message> {
    private static final MessageQueue instance = new MessageQueue();

    private MessageQueue() {
        super(20);
    }

    public static final MessageQueue getInstance() {
        return instance;
    }
}
