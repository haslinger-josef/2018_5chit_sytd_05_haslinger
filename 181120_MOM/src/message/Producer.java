package message;

public class Producer implements Runnable {
    @Override
    public void run() {
        Message message = new Message("This is a message!");

        try {
            MessageQueue.getInstance().put(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
