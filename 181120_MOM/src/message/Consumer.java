package message;

public class Consumer implements Runnable {
    @Override
    public void run() {
        try {
            Message message = MessageQueue.getInstance().take();
            System.out.println(message.getContent());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
