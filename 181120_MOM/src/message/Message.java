package message;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@RequiredArgsConstructor
@Data
public class Message implements Serializable {

    private String header;

    @NonNull
    private String content;
}
