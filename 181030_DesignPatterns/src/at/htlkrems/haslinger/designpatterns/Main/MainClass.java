package at.htlkrems.haslinger.designpatterns.Main;

import at.htlkrems.haslinger.designpatterns.decorater.Body;
import at.htlkrems.haslinger.designpatterns.decorater.Jeans;
import at.htlkrems.haslinger.designpatterns.decorater.TShirt;
import at.htlkrems.haslinger.designpatterns.singleton.RoboControl;
import at.htlkrems.haslinger.designpatterns.strategy.ABird;
import at.htlkrems.haslinger.designpatterns.strategy.Parrot;

public class MainClass {

        public static void main(String[] args) {
            RoboControl c = RoboControl.getInstance();
            System.out.println(c.getMessage());

            callStrategy();
            callDecorator();
        }

        public static void callStrategy()
        {
            ABird parrot = new Parrot();

            parrot.chirp();
            parrot.showColor();
        }
        public static void callDecorator()
        {
            Body body = new Body();
            Jeans jeans = new Jeans(body);
            TShirt tShirt = new TShirt(jeans);

            tShirt.printClothing();
        }
}
