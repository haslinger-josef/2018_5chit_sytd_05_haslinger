package at.htlkrems.haslinger.designpatterns.singleton;

public class RoboControl {
    private static final RoboControl instance = new RoboControl();

    private RoboControl(){}

    public static RoboControl getInstance()
    {
        return instance;
    }

    public String getMessage(){return "hello robo";}
}
