package at.htlkrems.haslinger.designpatterns.decorater;
public abstract class AClothingItem implements IClothing {
    protected IClothing previous;

    public AClothingItem(IClothing previous)
    {
        this.previous=previous;
    }
}
