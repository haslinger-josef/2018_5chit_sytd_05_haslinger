package at.htlkrems.haslinger.designpatterns.decorater;

public class Body implements IClothing {

        @Override
        public void printClothing() {
            System.out.println("Body");
        }
    }

