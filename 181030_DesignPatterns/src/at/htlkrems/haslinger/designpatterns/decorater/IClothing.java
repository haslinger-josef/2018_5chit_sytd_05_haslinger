package at.htlkrems.haslinger.designpatterns.decorater;

public interface IClothing {
    void printClothing();
}

