package at.htlkrems.haslinger.designpatterns.decorater;

public class Jeans extends AClothingItem {
    public Jeans(IClothing previous)
    {
        super(previous);
    }
    @Override
    public void printClothing() {
        System.out.println("jeans");
        super.previous.printClothing();
    }
}
