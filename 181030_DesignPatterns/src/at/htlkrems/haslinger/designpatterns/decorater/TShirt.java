package at.htlkrems.haslinger.designpatterns.decorater;

public class TShirt extends AClothingItem {
    public TShirt(IClothing previous)
    {
        super(previous);
    }
    @Override
    public void printClothing() {
        System.out.println("Socks");
        super.previous.printClothing();
    }
}
