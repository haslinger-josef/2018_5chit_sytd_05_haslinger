package at.htlkrems.haslinger.designpatterns.strategy;

public class DefaultStrategy implements IChirpStrategy {
    @Override
    public void chirp() {
        System.out.println("beep");;
    }
}
