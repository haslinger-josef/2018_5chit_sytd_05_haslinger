package at.htlkrems.haslinger.designpatterns.strategy;

public class Tucan extends ABird {
    public Tucan(){
        super(
                new DefaultStrategy(),
                new GreyStrategy()
        );
    }
}
