package at.htlkrems.haslinger.designpatterns.strategy;

public interface IShowColorStrategey {
    void showColor();
}
