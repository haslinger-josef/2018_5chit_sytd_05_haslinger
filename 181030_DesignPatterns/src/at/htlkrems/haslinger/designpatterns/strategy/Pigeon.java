package at.htlkrems.haslinger.designpatterns.strategy;

public class Pigeon extends ABird{
    public Pigeon(){
        super(
                new DefaultStrategy(),
                new ColerfulStrategy()
        );
    }
}
