package at.htlkrems.haslinger.designpatterns.strategy;

public interface IChirpStrategy {
    void chirp();

}
