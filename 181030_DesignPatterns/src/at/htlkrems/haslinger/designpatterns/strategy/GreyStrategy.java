package at.htlkrems.haslinger.designpatterns.strategy;

public class GreyStrategy implements IShowColorStrategey {
    @Override
    public void showColor() {
        System.out.println("grey");
    }
}
