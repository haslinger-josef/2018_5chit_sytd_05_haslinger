package at.htlkrems.haslinger.designpatterns.strategy;

public class Parrot extends ABird {
    public Parrot(){
        super(
                new TalkStrategy(),
                new ColerfulStrategy()
        );
    }
}
