package at.htlkrems.haslinger.designpatterns.strategy;

public abstract class ABird {
    private IChirpStrategy chirpStrategy;
    private IShowColorStrategey showColorStrategey;

    public ABird(IChirpStrategy cs, IShowColorStrategey scs){
        this.chirpStrategy = cs;
        this.showColorStrategey = scs;
    }

    public void chirp()
    {
        chirpStrategy.chirp();
    }
    public void showColor()
    {
        showColorStrategey.showColor();
    }
}
