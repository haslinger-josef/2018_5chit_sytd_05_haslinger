package at.htlkrems.library.service;

import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.model.AEntity;
import at.htlkrems.library.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

public abstract class ARessource <T extends AEntity, Id extends Serializable>{

    private static Logger log = LoggerFactory.getLogger(PingRessource.class);



    @Autowired
    private JpaRepository<T,Id> rep;

    @PostMapping(path="/createProject")
    @ResponseStatus(HttpStatus.CREATED)
    public T create(@RequestBody @Valid T t)
    {
        rep.save(t);
        log.info("created t");
        return t;
    }

    @PutMapping(path="/updateBook")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody @Valid T t)
    {
        rep.save(t);
        log.info("updated t");
    }

    @DeleteMapping(path="/deletedBook")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestBody @Valid T t)
    {
        rep.delete(t);
        log.info("deleted t");
    }

}
