package at.htlkrems.library.service;

import at.htlkrems.library.domain.IAuthorRepository;
import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.model.Author;
import at.htlkrems.library.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import javax.transaction.Transactional;
import javax.validation.Valid;

@Transactional
@RestController
@RequestMapping(path="/bookRessource")
public class BookRessource {

    private static Logger log = LoggerFactory.getLogger(PingRessource.class);


    @Autowired
    private IBookRepository rep;

    @Autowired
    private IAuthorRepository aurep;

    @PostMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Book create(@RequestBody @Valid Book b)
    {
        rep.save(b);
        log.info("created book "+ b.getDescription());
        return b;
    }

    @GetMapping(path = "{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.FOUND)
    public Book read(@PathVariable("id") Long bookID)
    {
        Book b =rep.findBookById(bookID);
        return b;
    }

    @PutMapping(path="/{bookID}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody @Valid Book b, @PathVariable("bookID") Long bookID)
    {
        rep.save(b);
        log.info("updated book" +bookID);
    }

    @DeleteMapping("/{bookID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("bookID") Long bookID)
    {
        rep.deleteById(bookID);
        log.info("deleted book" +bookID);
    }

    @GetMapping("/findBookByTitle")
    @ResponseStatus(HttpStatus.FOUND)
    public Book findBooksByTitle(@RequestParam("title") String title)
    {
        Book b = rep.findBookByTitle(title);
        return b;
    }


    @GetMapping("/countBooksFromAuthor")
    @ResponseStatus(HttpStatus.FOUND)
    public Long countBooksFromAuthor(@RequestParam("author") Long authorId)
    {
        Author a = aurep.findAuthorById(authorId);

        return rep.countBooksFromAuthor(a);
    }

    @GetMapping("/findBooksByTitleToken")
    @ResponseStatus(HttpStatus.FOUND)
    public BookListRepresentation findBooksByTitleContainsIgnoreCase(@RequestParam("token") String token)
    {
        return BookListRepresentation.create(rep.findBooksByTitleContainsIgnoreCase(token));
    }
}
