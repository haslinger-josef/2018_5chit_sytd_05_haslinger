package at.htlkrems.library.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/pingservice")
public class PingRessource {

    private static Logger log = LoggerFactory.getLogger(PingRessource.class);

    @GetMapping(path="/ping")
    public String ping(){
        log.info("ping");
        return "hallo welt";
    }
}
