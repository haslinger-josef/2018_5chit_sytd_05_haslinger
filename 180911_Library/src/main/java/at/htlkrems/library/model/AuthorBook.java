package at.htlkrems.library.model;


import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "AUTHOR_BOOK")
@NoArgsConstructor
@Getter
public class AuthorBook implements Serializable {

    @EmbeddedId
    private AuthorBookID authorBookID;

    @NotNull
    @Past
    @Column(name = "CREATED_AT",nullable = false)
    private Date createdAt;

    @NotNull
    @Size(min = 1)
    @Column(name = "EDITION",nullable = false)
    private Integer edition;

    public AuthorBook(Book book, Author author){
        authorBookID = new AuthorBookID(book, author);
    }
}
