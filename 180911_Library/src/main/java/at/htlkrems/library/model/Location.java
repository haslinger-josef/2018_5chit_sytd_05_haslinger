package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "LOCATION_TYPE")
@Entity
@Table(name = "LOCATION")

public class Location extends AEntity {

    @NotBlank
    @Size(max = 31)
    @Column(name = "CODE", nullable = false, unique = true, length = 31)
    private String code;
}
