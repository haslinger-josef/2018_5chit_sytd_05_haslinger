package at.htlkrems.library.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "LENDING")
@NoArgsConstructor
@Getter
public class Lending implements Serializable {
    @EmbeddedId
    private LendingID lendingID;

    @NotNull
    @Column(name = "LEND_AT", nullable = false)
    private Date lendAt;

    public Lending(Book book, Library library, LibraryUser libraryUser){
        lendingID = new LendingID(book, library, libraryUser);
    }
}
