package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter

@Entity
@DiscriminatorValue("SHELF")
public class Shelf extends Location{
}
