package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


@XmlRootElement(name = "category")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Entity
@Table(name = "CATEGORIES")
public class Category extends AEntity{

    @NotBlank
    @Size(max = 4000)
    @Column(name = "DESCRIPTION", nullable = false, length = 4000)
    private String description;

    @NotBlank
    @Size(max = 50)
    @Column(name = "NAME", nullable = false, unique = true, length = 50)
    private String name;

    @OneToMany
    @JoinColumn(name = "CATEGORY_ID")
    private List<Category> categories = new ArrayList<>();
}
