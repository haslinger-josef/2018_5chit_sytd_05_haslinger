package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter


@XmlRootElement(name = "author")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "AUTHORS")
public class Author extends Person {

    @NotBlank
    @Size(max = 255)
    @Column(nullable = false, unique = true)
    private String code;

    @NotBlank
    @Size(max = 4000)
    @Column(nullable = false, length = 4000)
    private String description;
}
