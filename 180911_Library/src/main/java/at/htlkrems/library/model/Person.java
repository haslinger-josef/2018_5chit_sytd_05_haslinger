package at.htlkrems.library.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY,property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value=Author.class,name="author"),
        @JsonSubTypes.Type(value=LibraryUser.class,name = "LibraryUser")})

@Entity
@Table(name ="PERSONS")
public class Person extends AEntity{


    @Past
    @NotBlank
    @Column(name = "BIRTHDAY", nullable = false)
    private Date birthday;

    @NotBlank
    @Size(max = 50)
    @Column(name = "GIVEN_NAME",nullable = false, length = 50)
    private String givenName;

    @NotBlank
    @Size(max = 255)
    @Column(name = "LAST_NAME",nullable = false)
    private String lastName;
}
