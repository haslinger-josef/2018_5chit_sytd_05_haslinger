package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "LIBRARY")
public class Library extends AEntity {

    @NotBlank
    @Size(max = 21)
    @Column(name = "CODE", nullable = false, unique = true, length = 21)
    private String code;

    @Size(max = 4000)
    @Column(name = "DESCRIPTION", length = 4000)
    private String description;

    @NotBlank
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @OneToMany
    @JoinColumn(name = "LOCATION_ID")
    private List<Location> locations = new ArrayList<>();
}
