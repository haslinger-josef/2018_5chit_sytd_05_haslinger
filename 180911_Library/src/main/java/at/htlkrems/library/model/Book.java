package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


//@XmlRootElement(name = "book")
//@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Entity
@Table(name = "BOOKS")
public class Book extends AEntity {

    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;

    @javax.validation.constraints.NotBlank
    @Size(max = 13)
    @Column(name = "ISBN", nullable = false, length = 13)
    private String isbn;

    @javax.validation.constraints.NotBlank
    @Size(max=255)
    @Column(name = "TITLE", nullable = false)
    private String title;

    @ManyToMany
    @JoinColumn(name = "BOOK_CATEGORY")
    private List<Category> categories = new ArrayList<>();
}
