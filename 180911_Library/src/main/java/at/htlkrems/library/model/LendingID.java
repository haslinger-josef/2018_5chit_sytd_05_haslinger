package at.htlkrems.library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class LendingID implements Serializable {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "BOOK_ID", updatable = false, nullable = false)
    private Book book;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "LIBRARY_ID", updatable = false, nullable = false)
    private Library library;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "LIBRARY_USER_ID", updatable = false, nullable = false)
    private LibraryUser libraryUser;


}
