package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter

@Entity
@DiscriminatorValue("ROOM")
public class Room extends Location{

    @OneToMany
    @JoinColumn(name = "SHELF_ID")
    private List<Shelf> shelves = new ArrayList<>();
}
