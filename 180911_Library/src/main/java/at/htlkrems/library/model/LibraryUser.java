package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter

@Entity
@Table(name = "LIBRARY_USER")
public class LibraryUser extends Person {

    @Size(max = 30)
    @NotBlank
    @Column(name = "USER_ID", nullable = false, unique = true, length = 30)
    private String userId;

    @Size(max = 255)
    @NotBlank
    @Column(name = "PSW", nullable = false)
    private String psw;
}
