package at.htlkrems.library.domain;

import at.htlkrems.library.model.Author;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.Set;

public interface IBookRepository extends JpaRepository<Book, Long> {

   Book findBookByTitle(String title);
    Book findBookById(Long id);

    Set<Book> findBooksByTitleContainsIgnoreCase(String token);

    Page<Book> findBookByTitleContainsIgnoreCaseOrderByTitleAsc(String token, Pageable page);

    Page<Book> findBookByDescriptionContainsIgnoreCaseOrderByTitleAsc(String token, Pageable page);


    @Query("select ab.authorBookID.book from AuthorBook ab where ab.authorBookID.author = :author ")
    Set<Book> findBooksByAuthor(@Param("author") Author author);

    @Query("select count (ab.authorBookID.book) from AuthorBook ab where ab.authorBookID.author = :author group by ab.authorBookID.author")
    Long countBooksFromAuthor(@Param("author") Author author);

    //@Query("select ab from ")
    //Set<Book> findBooksByCategory(@Param("category") Category category);


    @Query("select b from Book b join b.categories c on c in (select c2 from Category c1 join c1.categories c2 where c1 = :category) or c = :category")
    Set<Book> findBooksByCategoryOrderByTitle(@Param("category") Category category);
}
