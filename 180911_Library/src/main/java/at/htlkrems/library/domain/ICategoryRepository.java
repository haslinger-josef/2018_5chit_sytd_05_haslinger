package at.htlkrems.library.domain;

import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoryRepository extends JpaRepository<Category, Long> {

    Category findCategoryById(Long id);
}
