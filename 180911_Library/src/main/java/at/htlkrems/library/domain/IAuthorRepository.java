package at.htlkrems.library.domain;

import at.htlkrems.library.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IAuthorRepository extends JpaRepository<Author, Long> {

    Author findAuthorById(Long id);
}
