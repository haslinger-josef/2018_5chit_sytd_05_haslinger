insert into library (id, code, description, name) values (1, 'W20ZIK', 'Buecherei Zirkusgasse - Schwerpunkt: Fremdsprachige Bücher - Englisch, Russisch, Türkisch, Hebraeisch', 'Buecherei Zirkusgasse');

-- persons
insert into persons (id, birthday, given_name, last_name) values (1, '2000-01-13', 'Nora', 'Fabi');
insert into persons (id, birthday, given_name, last_name) values (2, '0200-01-16', 'Simon', 'Feiler');
insert into persons (id, birthday, given_name, last_name) values (3, '2018-02-03', 'Markus', 'Fichtinger');
insert into persons (id, birthday, given_name, last_name) values (4, '2018-02-22', 'Christoph', 'Hahn');
insert into persons (id, birthday, given_name, last_name) values (5, '2000-03-23', 'Josef', 'Haslinger');
insert into persons (id, birthday, given_name, last_name) values (6, '2000-04-22', 'Benjamin', 'Kasper');
insert into persons (id, birthday, given_name, last_name) values (7, '2018-09-16', 'Daniel', 'Kletzl');
insert into persons (id, birthday, given_name, last_name) values (8, '2018-09-21', 'Gregor', 'Langhammer');
insert into persons (id, birthday, given_name, last_name) values (9, '2018-09-22', 'Alexander', 'Schuster');
insert into persons (id, birthday, given_name, last_name) values (10, '2018-09-08', 'J.J.R.', 'Tolkien');
insert into persons (id, birthday, given_name, last_name) values (11, '2018-09-28', 'Herman', 'Melville');
insert into persons (id, birthday, given_name, last_name) values (12, '2018-09-28', 'John', 'Locke');
insert into persons (id, birthday, given_name, last_name) values (13, '2018-09-26', 'Khalil', 'Gibran');

-- library_user
insert into library_user (psw, user_id, id) values (1234, 'n.fabi', 1);
insert into library_user (psw, user_id, id) values (1234, 's.feiler', 2);
insert into library_user (psw, user_id, id) values (1234, 'm.fichtinger', 3);
insert into library_user (psw, user_id, id) values (1234, 'c.hahn', 4);
insert into library_user (psw, user_id, id) values (1234, 'j.haslinger', 5);
insert into library_user (psw, user_id, id) values (1234, 'b.kasper', 6);
insert into library_user (psw, user_id, id) values (1234, 'd.kletzl', 7);

-- authors
insert into authors (code, description, id) values ('g.lang', 'bla bla', 8);
insert into authors (code, description, id) values ('a.schuster', 'alora alora', 9);
insert into authors (code, description, id) values ('j.tolkien', 'Tolkien ist einer der bedeutensten Schriftsteller im Fantasy Bereich.', 10);
insert into authors (code, description, id) values ('h.melville', 'Herman Melville war ein amerikanischer Schriftsteller, Dichter und Essayist.', 11);
insert into authors (code, description, id) values ('j.locke', 'John Locke war ein englischer Arzt sowei ein einflussreicher Philosoph und Vordenker der Aufklärung', 12);
insert into authors (code, description, id) values ('k.gibran', 'Khalil Gibran war ein libanesich amerikanischer Maler, Philosoph und Dichter', 13);

-- books
insert into books (id, description, isbn, title) values (1, 'The Broken Wings is a poetic novel written by Khalil Gibran first published in Arabic in 1912. ', 1543235557, 'The broken Wings');
insert into books (id, description, isbn, title) values (4, 'Der Prophet (Originaltitel The Prophet) ist ein englischsprachiger literarisch-spiritueller Text des libanesisch-amerikanischen Dichters Khalil Gibran. ', 9176371123, 'Der Prophet (Originaltitel The Prophet) ist ein englischsprachiger literarisch-spiritueller Text des libanesisch-amerikanischen Dichters Khalil Gibran. ');
insert into books (id, description, isbn, title) values (6, 'The Wanderer', 54532164, 'The Wanderer');
insert into books (id, description, isbn, title) values (8, 'The Garden of the Prophet', 8545803245, 'The Garden of the Prophet');
insert into books (id, description, isbn, title) values (11, 'The Lord of the Rings is one of the most important ', 343423445, 'LOR: The Fellowship of the Ring');
insert into books (id, description, isbn, title) values (15, 'The Lord of the Rings is one of the most important ', 4234624542, 'LOR: The Two Towers');
insert into books (id, description, isbn, title) values (17, 'The Lord of the Rings is one of the most important ', 3423463451, 'LOR: The Return of the King');
insert into books (id, description, isbn, title) values (21, 'Silmarilion', 342343241, 'The Silmarilion');
insert into books (id, description, isbn, title) values (25, 'Moby Dick', 342436763, 'Moby Dick');
insert into books (id, description, isbn, title) values (30, 'Brief ueber die Toleranz', 32345234, 'Brief ueber die Toleranz');
insert into books (id, description, isbn, title) values (33, 'Of the Conduct of Understanding', 23232134, 'Of the Conduct of Understanding');
insert into books (id, description, isbn, title) values (999, 'test', 348436763, 'test');

-- book_authors
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-26', 4, 8, 10);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-10', 5, 11, 10);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-27', 2, 15, 10);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-26', 2, 17, 10);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-01', 2, 21, 10);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-02', 2, 25, 11);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-29', 2, 30, 12);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-07', 2, 33, 12);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-29', 3, 1, 13);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-18', 2, 4, 13);
insert into author_book (created_at, edition, book_id, author_id) values ('2018-09-04', 1, 6, 13);

-- categories
insert into categories (id, description, name, category_id) values (1, 'Belletristik', 'Belletristik', null);
insert into categories (id, description, name, category_id) values (2, 'Fachliteratur', 'Fachliteratur', null);
insert into categories (id, description, name, category_id) values (3, 'Ratgeber', 'Ratgeber', null);
insert into categories (id, description, name, category_id) values (4, 'Sachbuecher', 'Sachbuecher', null);
insert into categories (id, description, name, category_id) values (5, 'Comic', 'Comic', null);
insert into categories (id, description, name, category_id) values (6, 'Musiknoten', 'Musiknoten', null);
insert into categories (id, description, name, category_id) values (7, 'Krimis', 'Krimis', 1);
insert into categories (id, description, name, category_id) values (8, 'Thriller', 'Thriller', 1);
insert into categories (id, description, name, category_id) values (9, 'Romane', 'Romane', 1);
insert into categories (id, description, name, category_id) values (10, 'Erzaehlungen', 'Erzaehlungen', 1);
insert into categories (id, description, name, category_id) values (11, 'Fachbuecher', 'Fachbuecher', 2);
insert into categories (id, description, name, category_id) values (12, 'Sprachkurse', 'Sprachkurse', 2);
insert into categories (id, description, name, category_id) values (13, 'Computer und Internet', 'Computer und Internet', 3);
insert into categories (id, description, name, category_id) values (14, 'Ernaehrung', 'Ernaehrung', 3);
insert into categories (id, description, name, category_id) values (15, 'Haus und Garten', 'Haus und Garten', 3);
insert into categories (id, description, name, category_id) values (16, 'Reisefuehrer', 'Reisefuehrer', 3);
insert into categories (id, description, name, category_id) values (17, 'Biografien', 'Biografien', 4);
insert into categories (id, description, name, category_id) values (18, 'Business', 'Business', 4);
insert into categories (id, description, name, category_id) values (19, 'Film und Kunst', 'Film und Kunst', 4);
insert into categories (id, description, name, category_id) values (20, 'Politik', 'Politik', 4);
insert into categories (id, description, name, category_id) values (21, 'Religion', 'Religion', 4);

-- book_categories
insert into books_categories (categories_id, book_id) values (1, 1);
insert into books_categories (categories_id, book_id) values (10, 1);
insert into books_categories (categories_id, book_id) values (1, 4);
insert into books_categories (categories_id, book_id) values (10, 4);
insert into books_categories (categories_id, book_id) values (1, 6);
insert into books_categories (categories_id, book_id) values (10, 6);
insert into books_categories (categories_id, book_id) values (1, 8);
insert into books_categories (categories_id, book_id) values (10, 8);
insert into books_categories (categories_id, book_id) values (1, 11);
insert into books_categories (categories_id, book_id) values (9, 11);
insert into books_categories (categories_id, book_id) values (1, 15);
insert into books_categories (categories_id, book_id) values (9, 15);
insert into books_categories (categories_id, book_id) values (1, 17);
insert into books_categories (categories_id, book_id) values (9, 17);
insert into books_categories (categories_id, book_id) values (1, 21);
insert into books_categories (categories_id, book_id) values (9, 21);
insert into books_categories (categories_id, book_id) values (1, 25);
insert into books_categories (categories_id, book_id) values (9, 25);
insert into books_categories (categories_id, book_id) values (4, 30);
insert into books_categories (categories_id, book_id) values (20, 30);
insert into books_categories (categories_id, book_id) values (4, 33);
insert into books_categories (categories_id, book_id) values (20, 33);

insert into lending (lend_at, library_id, book_id, library_user_id) values ('2018-09-02 10:42:45', 1, 4, 1);
insert into lending (lend_at, library_id, book_id, library_user_id) values ('2016-09-17 10:41:54', 1, 1, 1);
insert into lending (lend_at, library_id, book_id, library_user_id) values ('2015-05-15 10:43:35', 1, 8, 1);
insert into lending (lend_at, library_id, book_id, library_user_id) values ('2014-12-15 10:45:41', 1, 15, 2);
insert into lending (lend_at, library_id, book_id, library_user_id) values ('2014-09-15 10:46:08', 1, 17, 2);
insert into lending (lend_at, library_id, book_id, library_user_id) values ('2014-09-15 10:44:49', 1, 11, 1);
insert into lending (lend_at, library_id, book_id, library_user_id) values ('2013-09-15 10:43:11', 1, 6, 1);