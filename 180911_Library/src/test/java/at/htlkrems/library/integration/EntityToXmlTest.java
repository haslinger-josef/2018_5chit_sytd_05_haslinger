package at.htlkrems.library.integration;


import at.htlkrems.library.domain.IAuthorRepository;
import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.domain.ICategoryRepository;
import at.htlkrems.library.model.Author;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import at.htlkrems.library.service.PingRessource;
import net.bytebuddy.asm.Advice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.xml.bind.JAXBException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = "test")

public class EntityToXmlTest {

    @Autowired
    private IBookRepository bookRepository;

    @Autowired
    private IAuthorRepository authorRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    @Test
    public void testBookXML() {
        Book book = bookRepository.getOne(1l);

        String xmlRepresentation = null;
        Book transformedBook = null;

        try {
            xmlRepresentation = book.toXML();
            transformedBook = Book.toEntity(xmlRepresentation, Book.class);
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }

        assertNotNull(transformedBook);
        assertEquals(book.getId(), transformedBook.getId());
        assertEquals(book.getTitle(), transformedBook.getTitle());
        assertEquals(book.getDescription(), transformedBook.getDescription());
        assertEquals(book.getIsbn(), transformedBook.getIsbn());
    }

    @Test
    public void testAuthorXML() {
        Author author = authorRepository.getOne(8l);

        String xmlRepresentation = null;
        Author transformedAuthor = null;

        try {
            xmlRepresentation = author.toXML();
            transformedAuthor = Author.toEntity(xmlRepresentation, Author.class);
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }

        assertNotNull(transformedAuthor);
        assertEquals(author.getId(), transformedAuthor.getId());
        assertEquals(author.getDescription(), transformedAuthor.getDescription());
        assertEquals(author.getLastName(), transformedAuthor.getLastName());

    }

    @Test
    public void testCategoryXML() {
        Category category = categoryRepository.getOne(8l);

        String xmlRepresentation = null;
        Category transformedCategory = null;

        try {
            xmlRepresentation = category.toXML();
            transformedCategory = Category.toEntity(xmlRepresentation, Category.class);
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }

        assertNotNull(transformedCategory);
        assertEquals(category.getId(), transformedCategory.getId());
        assertEquals(category.getDescription(), transformedCategory.getDescription());

    }
}
