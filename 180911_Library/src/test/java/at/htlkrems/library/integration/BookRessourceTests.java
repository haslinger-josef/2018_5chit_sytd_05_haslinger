package at.htlkrems.library.integration;


import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.service.BookListRepresentation;
import at.htlkrems.library.service.PingRessource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@Transactional

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = "test")


public class BookRessourceTests {

    @Autowired
    private IBookRepository repository;

    private static Logger log = LoggerFactory.getLogger(PingRessource.class);

    @Test
    public void createBook()
    {

        Book b = new Book();
        b.setDescription("This is a test book");
        b.setTitle("TestBook123");
        b.setIsbn("123456789");

        String requestUrl = "http://127.0.0.1:8181/library/bookRessource";

        RestTemplate restClient = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));





        HttpEntity<Book> request = new HttpEntity<>(b,headers);

        ResponseEntity<Book> response = restClient.postForEntity(requestUrl,request,Book.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void readBook()
    {
        String requestUrl = "http://127.0.0.1:8181/library/bookRessource/999";
        RestTemplate restClient = new RestTemplate();

        Book b = restClient.getForObject(requestUrl,Book.class);

        assertNotNull(b.getId());

    }

    @Test
    public void testUpdate()
    {
        Book b = repository.getOne(999l);
        assertNotNull(b);

        b.setTitle("Updated TestBook");

        String requestUrl = "http://127.0.0.1:8181/library/bookRessource/999";

        RestTemplate client = new RestTemplate();

        HttpEntity request = new HttpEntity<>(b);

       client.exchange(requestUrl,HttpMethod.PUT,request,Void.class);

        String request2Url = "http://127.0.0.1:8181/library/bookRessource/999";
        RestTemplate restClient = new RestTemplate();

        Book b2 = repository.getOne(999l);
        assertEquals(b2.getTitle(),"Updated TestBook");
    }

    @Test
    public void testDelete()
    {
        Book b = repository.getOne(999l);
        assertNotNull(b);

        String requestUrl = "http://127.0.0.1:8181/library/bookRessource/999";

        RestTemplate restClient = new RestTemplate();

        restClient.delete(requestUrl);

        try{
            Book b2 = repository.getOne(999l);
            assertNotNull(b);

        }
        catch(EntityNotFoundException cc)
        {
                log.info("works");
        }


    }

    @Test
    public void testFindBookByTitle()
    {
        String requestUrl = "http://127.0.0.1:8181/library/bookRessource/findBookByTitle?title=The Wanderer";
        RestTemplate restClient = new RestTemplate();

        Book b = restClient.getForObject(requestUrl,Book.class);

        assertNotNull(b);
    }

    @Test
    public void testCountBooksFromAuthor()
    {
        String requestUrl = "http://127.0.0.1:8181/library/bookRessource/countBooksFromAuthor?author=10";
        RestTemplate restClient = new RestTemplate();

        Long amount = restClient.getForObject(requestUrl,Long.class);



        assertEquals(new Long(5),amount);

    }

    @Test
    public void testFindBooksByTitleToken()
    {
        String requestUrl = "http://127.0.0.1:8181/library/bookRessource/findBooksByTitleToken?token=n";
        RestTemplate restClient = new RestTemplate();

        BookListRepresentation bookList = restClient.getForObject(requestUrl,BookListRepresentation.class);

        CharSequence token = "n";

        for (Book b1: bookList.getBooks()) {
            log.info(b1.getTitle());
            assertEquals(true,b1.getTitle().contains(token));
        }


    }
}
