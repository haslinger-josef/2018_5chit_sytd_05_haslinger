package at.htlkrems.library.Unit;

import at.htlkrems.library.domain.IAuthorRepository;
import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.domain.ICategoryRepository;
import at.htlkrems.library.model.Author;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BookUnitTest {

    private Logger log = LoggerFactory.getLogger(BookUnitTest.class);

    @Autowired
    private IBookRepository bookRepository;

    @Autowired
    private IAuthorRepository authorRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    @Test
    public void Test(){
        Book b = bookRepository.findBookByTitle("The Wanderer");
        assertNotNull(b);
        assertEquals("The Wanderer", b.getDescription());

        Set<Book> books = bookRepository.findBooksByTitleContainsIgnoreCase("the");
        assertNotNull(books);
        assertEquals(9,books.size());

        Page<Book> bookPage = bookRepository.findBookByTitleContainsIgnoreCaseOrderByTitleAsc("the", new PageRequest(0,10));
        assertNotNull(bookPage);
        assertEquals(1, bookPage.getTotalPages());
        assertEquals(9, bookPage.getTotalElements());


        Page<Book> bookPage1 = bookRepository.findBookByDescriptionContainsIgnoreCaseOrderByTitleAsc("Wanderer",  new PageRequest(0,10));
        assertNotNull(bookPage1);
        assertEquals(1, bookPage1.getTotalPages());
        assertEquals(1, bookPage1.getTotalElements());

        Author a = authorRepository.findAuthorById(10l);
        Set<Book> bookSet = bookRepository.findBooksByAuthor(a);
        assertNotNull(bookSet);
        assertEquals(5, bookSet.size());

        Long count = bookRepository.countBooksFromAuthor(a);
        assertNotNull(count);
        assertEquals(new Long(5), count);

        Category c = categoryRepository.findCategoryById(1l);

        Set<Book> bookSet2 = bookRepository.findBooksByCategoryOrderByTitle(c);
        assertNotNull(bookSet2);



        //assertEquals(2,bookSet2.size());

    }
}
