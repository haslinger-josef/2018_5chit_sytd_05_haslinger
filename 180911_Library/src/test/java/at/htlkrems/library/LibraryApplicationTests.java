package at.htlkrems.library;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LibraryApplicationTests {

	private static Logger log = LoggerFactory.getLogger(LibraryApplicationTests.class);

	@Test
	public void contextLoads() {
		log.info("Hallo Welt");
	}

}
