public interface IIngredient{
    Integer getPrize();
    String getName();
}
