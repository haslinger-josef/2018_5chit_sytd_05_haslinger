public class Dough implements IIngredient {

    @Override
    public Integer getPrize() {
        return 1;
    }

    @Override
    public String getName() {
        return "Dough";
    }
}
