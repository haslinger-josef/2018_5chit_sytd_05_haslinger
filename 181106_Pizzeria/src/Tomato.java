public class Tomato extends AIngredient {

    public Tomato(IIngredient prev) {
        super(prev,1, "Tomato");
    }
}
