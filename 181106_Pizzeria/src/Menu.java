import java.util.ArrayList;
import java.util.List;

public class Menu implements IMenuItem {

    List<IMenuItem> menu = new ArrayList<>();

    String name;

    public Menu(String name)
    {
        this.name=name;
    }

    public void add(IMenuItem add)
    {
        menu.add(add);
    }

    @Override
    public String print() {

        String outPut ="";
        outPut +=  name+" contains:\n";

        for (IMenuItem item:menu) {
            outPut += item.print()+"\n";
        }
        return outPut;
    }
}
