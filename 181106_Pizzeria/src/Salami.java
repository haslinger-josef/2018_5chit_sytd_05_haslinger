public class Salami extends AIngredient {

    public Salami(IIngredient prev) {
        super(prev,4, "Salami");
    }
}
