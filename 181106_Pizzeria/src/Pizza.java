public class Pizza implements IMenuItem {

    IIngredient pizza;
    String name;
    public Pizza(IIngredient pizza,String name)
    {
        this.pizza=pizza;
        this.name=name;
    }
    @Override
    public String print() {
        return String.format("%s (%d€)\n\t%s", this.name, this.pizza.getPrize(), this.pizza.getName());
    }
}
