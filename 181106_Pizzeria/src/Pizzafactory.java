public class Pizzafactory {
    Pizza createMageritha()
    {
        return new Pizza(new Cheese(new Dough()),"Mageritha");
    }

    Pizza createSalami()
    {
        return new Pizza(new Cheese(new Tomato(new Salami(new Dough()))),"Salami");
    }

    Pizza createHotsalami()
    {
        return new Pizza(new Cheese(new Tomato(new Salami(new Chili(new Dough())))),"HotSalami");
    }
}
