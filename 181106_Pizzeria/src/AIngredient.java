public abstract class AIngredient implements IIngredient {
    IIngredient previous;
    Integer prize;
    String name;

    public AIngredient(IIngredient prev, Integer prize, String name)
    {
        this.name=name;
        this.prize=prize;
        this.previous=prev;
    }

    @Override
    public Integer getPrize() {
        return this.prize + previous.getPrize();
    }

    @Override
    public String getName() {
        return this.name +  " " + previous.getName();
    }

}
