public class Cheese extends AIngredient {

    public Cheese(IIngredient prev) {
        super(prev,3, "Cheese");
    }
}
