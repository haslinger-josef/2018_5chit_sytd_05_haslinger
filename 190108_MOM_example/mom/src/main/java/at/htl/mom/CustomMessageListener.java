package at.htl.mom;

import at.htl.transport.MomApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class CustomMessageListener {

    private static final Logger log = LoggerFactory.getLogger(CustomMessageListener.class);

    @RabbitListener(queues = MomApplication.QUEUE_GENERIC_NAME)
    public void receiveMessage(final CustomMessage message) {
        log.info("Received message as generic: {}", message.toString());
    }
}
