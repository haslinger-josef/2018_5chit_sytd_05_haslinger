package at.htl.transport;

public enum EType {
    SCHUETTGUT,
    CONTAINER,
    SILOGUT,
    STAPELGUT
}
