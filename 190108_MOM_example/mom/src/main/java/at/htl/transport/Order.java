package at.htl.transport;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class Order implements Serializable {
    public int id;
    public String name;
    public EType type;

    public Order(@JsonProperty("id") int id,
                 @JsonProperty("name") String name,
                 @JsonProperty("type") EType type)
    {
        this.id=id;
        this.name = name;
        this.type = type;
    }

    public String toRoutingKey(){
        return this.type.toString().toLowerCase();
    }
}

