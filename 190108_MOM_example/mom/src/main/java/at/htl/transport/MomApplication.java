package at.htl.transport;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MomApplication {

    public final static String EXCHANGE_NAME = "rabConnExchange";
    public final static String QUEUE_GENERIC_NAME = "rabConnQueue";
    public final static String ROUTING_KEY = "";

    public static void main(String[] args) {
        SpringApplication.run(MomApplication.class, args);


    }

}

