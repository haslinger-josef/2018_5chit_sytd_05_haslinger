package at.htl.transport;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class OrderProducer {
    private final RabbitTemplate rabbitTemplate;
    private static final String EXCHANGE_NAME = "auftrag.exchange";
    private static Integer lastID = 0;

    public OrderProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 5000L)
    public void sendMessage() {
        EType type = EType.STAPELGUT;
        switch (++lastID % 4) {
            case 0:
                type = EType.STAPELGUT;
                break;
            case 1:
                type = EType.CONTAINER;
                break;
            case 2:
                type = EType.SILOGUT;
                break;
            default:
                type = EType.SCHUETTGUT;
                break;

        }
        Order order = new Order(lastID,"Message #"+lastID.toString(),type);
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, order.toRoutingKey(), order);
        System.out.println(order.toRoutingKey());
    }

}
