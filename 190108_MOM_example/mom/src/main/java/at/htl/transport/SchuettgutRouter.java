package at.htl.transport;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class SchuettgutRouter {

    private final RabbitTemplate rabbitTemplate;
    private static final String EXCHANGE_NAME = "route.exchange";
    private static Integer lastID = 0;

    public SchuettgutRouter(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
    @RabbitListener(queues = "schuettgut.auftrag")
    public void route(Order order)
    {
        System.out.println("Order: "+order.name+" routet");
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, order.toRoutingKey(), order);
    }
}
